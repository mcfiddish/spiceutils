#!/bin/bash

# Configure these for the package
package=SPICEUtils
srcFile="./src/main/java/spiceutils/utils/AppVersion.java"
appSrcDir='spiceutils/apps'

function build_jar() {
    cwd=$(pwd)
    cd $(dirname $0)

    date=$(date -u +"%Y-%b-%d %H:%M:%S %Z")

    if [ -d .git ]; then
	rev=$(git rev-parse --verify --short HEAD)
    else
	rev="UNVERSIONED"
    fi
    
    jarFile=${package}-${rev}.jar

    # store the version number in pom.xml
    cp -p pom.xml pom.bak
    sed "s/0.0.1-SNAPSHOT/$rev/g" pom.bak > pom.xml

    touch $srcFile

    cat <<EOF > $srcFile

package spiceutils.utils;

public class AppVersion {
    public final static String gitRevision = new String("$rev");
	public final static String applicationName = new String("$package");
	public final static String dateString = new String("$date");

	private AppVersion() {}

	/**
	* version $rev (built $date)
	*/
	public static String getVersionString() {
                return String.format("%s version %s (built %s)", applicationName, gitRevision, dateString);
	}
}
EOF

    # install the SPICE jar file
    mvn install:install-file
    mvn clean install

    # restore the old pom file
    mv pom.bak pom.xml

    cd $cwd
}

function make_scripts() {
    cwd=$(pwd)

    mkdir -p ${binDir}
    mkdir -p ${libDir}
    rsync -a target/${package}.jar ${libDir}
    rsync -a target/${package}_lib ${libDir}
    rsync -a lib/Darwin_x86_64 ${libDir}
    rsync -a lib/Linux_x86_64 ${libDir}

    for class in $(jar tf target/${package}.jar  | grep $appSrcDir | grep -v '\$' | grep class); do
	base=$(basename $class ".class")
	tool=${binDir}/${base}
	path=$(dirname $class | sed 's,/,.,g').${base}
	echo "#!/bin/bash" > ${tool}
	echo 'root=$(dirname $0)' >> ${tool}
	echo 'MEMSIZE=""' >> ${tool}
	echo 'NATIVELIBS=$(uname -s)_$(uname -m)' >> ${tool}
	echo 'if [ "$(uname)" == "Darwin" ]; then' >> ${tool}
	echo '    MEMSIZE=$(sysctl hw.memsize | awk '\''{print int($2/1024)}'\'')'  >> ${tool}
	echo '    export DYLD_LIBRARY_PATH="${root}/../lib/${NATIVELIBS}:$DYLD_LIBRARY_PATH"'  >> ${tool}
	echo 'elif [ "$(uname)" == "Linux" ]; then' >> ${tool}
	echo '    MEMSIZE=$(grep MemTotal /proc/meminfo | awk '\''{print $2}'\'')' >> ${tool}
	echo '    export LD_LIBRARY_PATH="${root}/../lib/${NATIVELIBS}:$LD_LIBRARY_PATH"'  >> ${tool}
	echo 'fi' >> ${tool}
	echo "java -Xmx\${MEMSIZE}K -cp \${root}/../lib/*:\${root}/../lib/${package}_lib/* $path \$@" >> ${tool}

	chmod +x ${tool}
    done
    cd $cwd
}

function make_doc {
    cwd=$(pwd)

    mkdir -p $docDir

    # build javadoc
    lombok=$(find ${libDir} -name 'lombok*.jar')
    java -jar $lombok delombok ${cwd}/src/main/java -d ${docDir}/src -c ${libDir}/*:${libDir}/${package}_lib/*
    javadoc -quiet -Xdoclint:none -cp ${libDir}/*:${libDir}/${package}_lib/* -d ${docDir}/javadoc -sourcepath ${docDir}/src/ -subpackages spiceutils -overview ${docDir}/src/overview.html
    /bin/rm -fr ${docDir}/src

    html=$docDir/index.html

    echo '<html>' > $html
    echo '<h1>SPICE Utils</h1>' >> $html
    echo '<ul>' >> $html

    for file in ${binDir}/*; do
	base=$(basename $file)
	echo '<li><a href="#'$base'">'$base'</a> - '$($file -shortDescription | sed 's/</\&lt;/g' | sed 's/>/\&gt;/g')'</li>' >> $html
    done

    echo '</ul>' >> $html

    for file in ${binDir}/*; do
	base=$(basename $file)
	echo '<h2 id="'$base'">'$base'</h2>' >> $html
	echo '<pre>' >> $html
	$file | sed 's/</\&lt;/g' | sed 's/>/\&gt;/g' >> $html
	echo '</pre>' >> $html
    done

    echo '</html>' >> $html

    cd $cwd
}

### Don't need to modify anything below this line

root=${package}-$(date -u +"%Y.%m.%d")
binDir=${root}/scripts
libDir=${root}/lib
docDir=${root}/doc

java=$(which java)
if [ -z $java ]; then
    echo "Java executable not found in your PATH"
    exit 1
fi
fullVersion=$(java -version 2>&1 | head -1 |awk -F\" '{print $2}')
version=$(echo $fullVersion | awk -F\. '{print $1}')
if [ "$version" -lt "11" ];then
    echo "minimum Java version required is 11.  Version found is $fullVersion."
    exit 1
fi

# Build the jar file
build_jar

# create the executable scripts
make_scripts

# create documentation
make_doc

mkdir -p dist
rsync -a mkPackage.bash pom.xml src ${root}-src/
tar cfz ./dist/${root}.tar.gz ./${root}
tar cfz ./dist/${root}-src.tar.gz ./${root}-src

echo -e "\nCreated ./dist/${root}.tar.gz ./dist/${root}-src.tar.gz"

/bin/rm -fr ${root} ./${root}-src

if [ -d .git ]; then
    git restore $srcFile
fi
