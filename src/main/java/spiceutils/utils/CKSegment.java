package spiceutils.utils;

import lombok.Builder;
import lombok.Getter;
import spice.basic.Instrument;
import spice.basic.ReferenceFrame;
import spice.basic.SCLKTime;

@Getter
public class CKSegment {

	private int index;
	private SCLKTime first;
	private SCLKTime last;
	private Instrument inst;
	private ReferenceFrame frame;
	private boolean avflag;
	private String segid;

	@Builder
	private CKSegment(int index, SCLKTime first, SCLKTime last, Instrument inst, ReferenceFrame frame, boolean avflag,
			String segid) {
		this.index = index;
		this.first = first;
		this.last = last;
		this.inst = inst;
		this.frame = frame;
		this.avflag = avflag;
		this.segid = segid;
	}

}
