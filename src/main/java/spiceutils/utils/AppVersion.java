
package spiceutils.utils;

public class AppVersion {
    public final static String gitRevision = new String("4a181ff");
	public final static String applicationName = new String("SPICEUtils");
	public final static String dateString = new String("2021-Mar-16 22:28:20 UTC");

	private AppVersion() {}

	/**
	* version 4a181ff (built 2021-Mar-16 22:28:20 UTC)
	*/
	public static String getVersionString() {
                return String.format("%s version %s (built %s)", applicationName, gitRevision, dateString);
	}
}
