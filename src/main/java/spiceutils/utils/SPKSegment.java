package spiceutils.utils;

import lombok.Builder;
import lombok.Getter;
import spice.basic.Body;
import spice.basic.ReferenceFrame;
import spice.basic.TDBTime;

@Getter
public class SPKSegment {

	private int index;
	private Body target;
	private Body center;
	private ReferenceFrame frame;
	private TDBTime first;
	private TDBTime last;
	private String segid;
	private int degree;

	@Builder
	private SPKSegment(int index, Body target, Body center, ReferenceFrame frame, TDBTime first, TDBTime last,
			String segid, int degree) {
		this.index = index;
		this.target = target;
		this.center = center;
		this.frame = frame;
		this.first = first;
		this.last = last;
		this.segid = segid;
		this.degree = degree;
	}

}
