package spiceutils.templates;

import org.apache.commons.cli.Options;

public interface SPICETool {
	/**
	 * @return One line description of this tool
	 */
	String shortDescription();

	/**
	 * @return Complete description of this tool
	 */
	String fullDescription(Options options);
}
