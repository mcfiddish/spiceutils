package spiceutils.templates;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spiceutils.utils.AppVersion;

public class DefaultSPICETool implements SPICETool {

  private static DefaultSPICETool defaultOBJ = new DefaultSPICETool();
  private static final Logger logger = LogManager.getLogger();

  private DefaultSPICETool() {}

  @Override
  public String shortDescription() {
    return "SHORT DESCRIPTION.";
  }

  @Override
  public String fullDescription(Options options) {
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);

    String header = "";
    String footer = "\nFULL DESCRIPTION.\n";
    pw.println(AppVersion.getVersionString() + "\n");
    HelpFormatter formatter = new org.apache.commons.cli.HelpFormatter();
    formatter.printHelp(pw, formatter.getWidth(),
        String.format("%s [options]", this.getClass().getSimpleName()), header, options,
        formatter.getLeftPadding(), formatter.getDescPadding(), footer);
    pw.flush();
    return sw.toString();
  }

  public static void main(String[] args) {

    Options options = new Options();
    options.addOption(Option.builder("logFile").hasArg()
        .desc("If present, save screen output to log file.").build());
    options.addOption(Option.builder("logLevel").hasArg()
        .desc("If present, print messages above selected priority.  Valid values are "
            + "ALL, OFF, SEVERE, WARNING, INFO, CONFIG, FINE, FINER, or FINEST.")
        .build());

    // if no arguments, print the usage and exit
    if (args.length == 0) {
      System.out.println(defaultOBJ.fullDescription(options));
      System.exit(0);
    }

    // if -shortDescription is specified, print short description and exit.
    for (String arg : args) {
      if (arg.equals("-shortDescription")) {
        System.out.println(defaultOBJ.shortDescription());
        System.exit(0);
      }
    }

    // parse the arguments
    CommandLine cl = null;
    try {
      cl = new DefaultParser().parse(options, args);
    } catch (ParseException e) {
      System.out.println(e.getMessage());
      System.out.println(defaultOBJ.fullDescription(options));
      System.exit(0);
    }

    String hostname = "unknown host";
    try {
      hostname = InetAddress.getLocalHost().getHostName();
    } catch (UnknownHostException e) {
    }

    // if (cl.hasOption("logLevel"))
    // logger.setLogLevel(Level.parse(cl.getOptionValue("logLevel").toUpperCase().trim()));
    // if (cl.hasOption("logFile"))
    // logger.setLogFile(cl.getOptionValue("logFile"));

    logger.info(String.format("Start %s + [%s] on %s", defaultOBJ.getClass().getSimpleName(),
        AppVersion.getVersionString(), hostname));
    StringBuilder sb = new StringBuilder();
    sb.append("arguments: ");
    for (String arg : args)
      sb.append(String.format("%s ", arg));
    logger.info(sb.toString());

  }

}
