package spiceutils.apps;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.function.Function;
import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.solvers.BisectionSolver;
import org.apache.commons.math3.analysis.solvers.UnivariateSolver;
import org.apache.commons.math3.util.FastMath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spice.basic.AberrationCorrection;
import spice.basic.Body;
import spice.basic.KernelDatabase;
import spice.basic.LatitudinalCoordinates;
import spice.basic.PositionVector;
import spice.basic.RADecCoordinates;
import spice.basic.ReferenceFrame;
import spice.basic.SpiceErrorException;
import spice.basic.SpiceException;
import spice.basic.StateRecord;
import spice.basic.TDBTime;

/**
 * Given a time, this class finds times of equinox and equal solar longitude in subsequent years.
 * 
 * @author mcfid
 *
 */
public class Calendar {

  static {
    System.loadLibrary("JNISpice");
  }

  private Map<Integer, Double> vernalEquinoxes;
  private Map<Integer, Double> anniversaries;

  private Body center;
  private ReferenceFrame frame;
  private UnivariateFunction baseFunc;

  private static final Logger logger = LogManager.getLogger();

  /**
   * 
   * @param sunCenter if true, coordinate system center is the Sun, otherwise use the solar system
   *        barycenter
   * @param trueEquator if true, use true equator (account for nutation), otherwise use mean equator
   *        (ignore nutation)
   * @param tropical if true, use equinox of date (tropical year), otherwise use J2000 (sidereal
   *        year)
   */
  public Calendar(boolean sunCenter, boolean trueEquator, boolean tropical) {
    try {
      this.center = sunCenter ? new Body("SUN") : new Body(0);
      this.frame = trueEquator ? new ReferenceFrame("TRUE399") : new ReferenceFrame("MEAN399");
      this.baseFunc = tropical ? this.centerLongitudeEquatorOfDate(frame) : this.centerRAJ2000();
    } catch (SpiceException e) {
      e.printStackTrace();
    }

    vernalEquinoxes = new HashMap<>();
    anniversaries = new HashMap<>();
  }

  /**
   * 
   * @param startYear
   * @param stopYear
   * @param t reference time
   */
  public void run(int startYear, int stopYear, double t) {
    try {

      // decide to evaluate from -pi to pi or 0 to 2*pi
      // default func returns from 0 to 2*pi
      final boolean minusPiToPi = FastMath.cos(baseFunc.value(t)) > 0;
      UnivariateFunction func = new UnivariateFunction() {

        @Override
        public double value(double x) {
          double lon = baseFunc.value(x);
          if (minusPiToPi)
            if (lon > Math.PI)
              return lon - 2 * Math.PI;
          return lon;
        }

      };

      NavigableSet<Double> searchRanges = new TreeSet<>();
      for (int year = startYear; year < stopYear + 2; year++) {
        TDBTime tdb;
        tdb = new TDBTime(String.format("%04d JUN 20 00:00:00", year));
        double et = tdb.getTDBSeconds();
        double c = getLastZero(et, sinOf(func));
        vernalEquinoxes.put(year, c);
        searchRanges.add(c);
      }

      // evaluate longitude - desired longitude at specified time. We want the value of x when func2
      // is zero.
      UnivariateFunction func2 = new UnivariateFunction() {

        @Override
        public double value(double x) {
          return func.value(x) - func.value(t);
        }

      };

      // guess at the right time for each year
      NavigableSet<Double> equinoxesTDB = new TreeSet<>(vernalEquinoxes.values());
      double timeOffset = t - equinoxesTDB.floor(t);

      for (int year : vernalEquinoxes.keySet()) {
        Double prev = vernalEquinoxes.get(year);
        double absoluteAccuracy = 1e-6;
        UnivariateSolver solver = new BisectionSolver(absoluteAccuracy);
        // search range is +/- 30 days
        double p = prev + timeOffset - 30 * 86400;
        double n = p + 60 * 86400;
        double c = solver.solve(100, func2, p, n, func2.value(prev + timeOffset));
        anniversaries.put(year, c);

      }

    } catch (SpiceErrorException e) {
      e.printStackTrace();
    }

  }

  /**
   * 
   * @return {@link Map} of year to equinox time
   */
  public Map<Integer, Double> getEquinoxes() {
    return Collections.unmodifiableMap(vernalEquinoxes);
  }

  /**
   * 
   * @return{@link Map} of year to anniversary time
   */
  public Map<Integer, Double> getAnniversaries() {
    return Collections.unmodifiableMap(anniversaries);
  }

  /**
   * 
   * @param frame
   * @return {@link Function} returning center's position as {@link LatitudinalCoordinates} relative
   *         to Earth as a function of time
   */
  private Function<Double, LatitudinalCoordinates> earthToCenterEquatorOfDate(
      ReferenceFrame frame) {
    return new Function<Double, LatitudinalCoordinates>() {

      @Override
      public LatitudinalCoordinates apply(Double t) {
        try {
          StateRecord earthRecordJ2000 = new StateRecord(center, new TDBTime(t), frame,
              new AberrationCorrection("LT+S"), new Body("EARTH"));
          PositionVector earthPosJ2000 = earthRecordJ2000.getPosition();
          return new LatitudinalCoordinates(earthPosJ2000);
        } catch (SpiceException e) {
          e.printStackTrace();
        }
        return null;
      }

    };

  }

  private Function<Double, RADecCoordinates> earthToCenterJ2000() {
    return new Function<Double, RADecCoordinates>() {

      @Override
      public RADecCoordinates apply(Double t) {
        try {
          StateRecord earthRecordJ2000 = new StateRecord(center, new TDBTime(t),
              new ReferenceFrame("J2000"), new AberrationCorrection("LT+S"), new Body("EARTH"));
          PositionVector earthPosJ2000 = earthRecordJ2000.getPosition();
          return new RADecCoordinates(earthPosJ2000);
        } catch (SpiceException e) {
          e.printStackTrace();
        }
        return null;
      }

    };
  }

  /**
   * 
   * @param frame
   * @return longitude of center's position relative to Earth in the desired {@link ReferenceFrame}.
   *         Units are radians, range is [0, 2*PI]
   */
  private UnivariateFunction centerLongitudeEquatorOfDate(ReferenceFrame frame) {
    return new UnivariateFunction() {

      @Override
      public double value(double x) {
        LatitudinalCoordinates latLon = earthToCenterEquatorOfDate(frame).apply(x);
        if (latLon == null)
          return Double.NaN;
        double lon = latLon.getLongitude();
        if (lon < 0)
          lon += 2 * Math.PI;
        return lon;
      }

    };

  }

  /**
   * Find the right ascension of the central body in J2000 seen from the Earth. Units are radians,
   * range is [0, 2*PI]
   * 
   * @return
   */
  private UnivariateFunction centerRAJ2000() {
    return new UnivariateFunction() {

      @Override
      public double value(double x) {
        RADecCoordinates raDec = earthToCenterJ2000().apply(x);
        return raDec == null ? Double.NaN : raDec.getRightAscension();
      }

    };
  }

  private UnivariateFunction sinOf(UnivariateFunction func) {
    return new UnivariateFunction() {

      @Override
      public double value(double x) {
        return FastMath.sin(func.value(x));
      }

    };

  }

  /**
   * Check for the last zero of the function - also check that it's close to northern spring equinox
   * 
   * @param t
   * @param function
   * @return
   */
  private double getLastZero(double t, UnivariateFunction function) {
    double absoluteAccuracy = 1e-6;
    UnivariateSolver solver = new BisectionSolver(absoluteAccuracy);
    int nDays = 1;
    double min = t - nDays * 86400;
    double max = t + nDays * 86400;
    double c = solver.solve(100, function, min, max, t);
    while (Math.abs(function.value(c)) > absoluteAccuracy) {
      nDays += nDays;
      min -= nDays * 86400;
      c = solver.solve(100, function, min, max, t);
    }

    // check that cos(center's RA) is positive
    RADecCoordinates raDec = earthToCenterJ2000().apply(c);
    if (Math.cos(raDec.getRightAscension()) < 0) {
      nDays = 1;
      max = c - nDays * 86400;
      min = max - nDays * 86400;
      c = min;
      while (Math.abs(function.value(c)) > absoluteAccuracy) {
        nDays += nDays;
        min -= nDays * 86400;
        c = solver.solve(100, function, min, max, t);
      }
      raDec = earthToCenterJ2000().apply(c);
    }

    return c;
  }

  /**
   * Write a jar resource to a temporary file.
   * 
   * @param path
   * @return
   */
  private static String writeResource(String path) {
    String resourcePath = null;
    try (InputStream input = Calendar.class.getResourceAsStream(path)) {
      File file = File.createTempFile("calendar", ".tmp");
      try (OutputStream out = new FileOutputStream(file)) {
        int read;
        byte[] bytes = new byte[1024];
        while ((read = input.read(bytes)) != -1) {
          out.write(bytes, 0, read);
        }
      }
      file.deleteOnExit();
      resourcePath = new String(file.getAbsolutePath());
    } catch (IOException ex) {
      System.err.println(ex.getLocalizedMessage());
    }
    return resourcePath;
  }

  public static void main(String[] args) throws SpiceException {

    List<String> kernels = new ArrayList<>();
    kernels.add(writeResource("/spice/naif0012.tls"));
    kernels.add(writeResource("/spice/frames.tf"));
    kernels.add(writeResource("/spice/de421.bsp"));

    for (String kernel : kernels)
      KernelDatabase.load(kernel);

    int startYear = 1969;
    int stopYear = 2025;
    double t = new TDBTime("1969 APR 18 14:35:00").getTDBSeconds();
    // t = new TDBTime("2005 OCT 21 08:46:00").getTDBSeconds();

    boolean sunCenter = true;
    boolean trueEquator = true;
    boolean tropical = true;

    Calendar cal = new Calendar(sunCenter, trueEquator, tropical);
    cal.run(startYear, stopYear, t);

    Map<Integer, Double> equinoxes = cal.getEquinoxes();
    Map<Integer, Double> anniversaries = cal.getAnniversaries();
    String equinoxString = "Spring equinox";
    String anniversaryString = (tropical ? "Tropical" : "Sidereal") + " anniversary";
    String longitudeString =
        String.format("Solar longitude (%s)", tropical ? cal.frame.getName() : "J2000");
    System.out.printf("Year %24s %24s %s\n", equinoxString, anniversaryString, longitudeString);
    for (int year = startYear; year <= stopYear; year++) {
      Double equinox = equinoxes.get(year);
      if (equinox == null)
        continue;
      Double anniversary = anniversaries.get(year);
      if (anniversary == null)
        continue;
      System.out.printf("%4d %24s %24s %f\n", year, new TDBTime(equinox).toUTCString("ISOC", 3),
          new TDBTime(anniversary).toUTCString("ISOC", 3),
          Math.toDegrees(cal.baseFunc.value(anniversary)));
    }

    KernelDatabase.clear();
  }
}
