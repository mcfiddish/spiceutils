package spiceutils.apps;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Comparator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import lombok.Getter;
import spice.basic.AberrationCorrection;
import spice.basic.Body;
import spice.basic.KernelDatabase;
import spice.basic.Matrix33;
import spice.basic.ReferenceFrame;
import spice.basic.SPK;
import spice.basic.SpiceErrorException;
import spice.basic.SpiceException;
import spice.basic.SpiceQuaternion;
import spice.basic.SpiceWindow;
import spice.basic.StateVector;
import spice.basic.TDBTime;
import spice.basic.TimeConstants;
import spice.basic.Vector3;
import spiceutils.templates.SPICETool;
import spiceutils.utils.AppVersion;

public class CreateCK implements SPICETool {
  private static CreateCK defaultOBJ = new CreateCK();

  private CreateCK() {}

  @Override
  public String shortDescription() {
    return "Write MSOPCK input files from primary and secondary pointing constraints.";
  }

  @Override
  public String fullDescription(Options options) {
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);

    // @formatter:off
		String header = "";
		String footer = "\nThis program will create input files for MSOPCK from primary and secondary pointing constraints.\n";
		// @formatter:on
    pw.println(AppVersion.getVersionString() + "\n");
    HelpFormatter formatter = new org.apache.commons.cli.HelpFormatter();
    formatter.printHelp(pw, formatter.getWidth(),
        String.format("%s [options]", this.getClass().getSimpleName()), header, options,
        formatter.getLeftPadding(), formatter.getDescPadding(), footer);
    pw.flush();
    return sw.toString();
  }

  @Getter
  private enum AXIS {
    X(1), Y(2), Z(3), MINUS_X(-1), MINUS_Y(-2), MINUS_Z(-3);

    int index;

    private AXIS(int index) {
      this.index = index;
    }

  };

  private String inputSPK;
  private Body spacecraft;
  private static final Logger logger = LogManager.getLogger();

  public CreateCK(String inputSPK, Body spacecraft) {
    this.inputSPK = inputSPK;
    this.spacecraft = spacecraft;
  }

  private SpiceQuaternion getAttitude(TDBTime tdb, AXIS primaryAxis, Body primaryTarget,
      boolean primaryVelocity, AXIS secondaryAxis, Body secondaryTarget, boolean secondaryVelocity)
      throws SpiceException {

    ReferenceFrame J2000 = new ReferenceFrame("J2000");
    AberrationCorrection abcorr = new AberrationCorrection("LT+S");
    StateVector primary = new StateVector(primaryTarget, tdb, J2000, abcorr, spacecraft);
    StateVector secondary = new StateVector(secondaryTarget, tdb, J2000, abcorr, spacecraft);

    Vector3 primaryVector =
        primaryVelocity ? primary.getVelocity().negate() : primary.getPosition();
    Vector3 secondaryVector =
        secondaryVelocity ? secondary.getVelocity().negate() : secondary.getPosition();

    int primaryIndex = primaryAxis.getIndex();
    if (primaryIndex < 0)
      primaryVector = primaryVector.negate();

    int secondaryIndex = secondaryAxis.getIndex();
    if (secondaryIndex < 0)
      secondaryVector = secondaryVector.negate();

    Matrix33 j2000ToSc = new Matrix33(primaryVector, Math.abs(primaryIndex), secondaryVector,
        Math.abs(secondaryIndex));
    return new SpiceQuaternion(j2000ToSc);
  }

  public String createCK(String lsk, double maxStep, int numStates, AXIS primaryAxis,
      Body primaryTarget, boolean primaryVelocity, AXIS secondaryAxis, Body secondaryTarget,
      boolean secondaryVelocity) throws SpiceException {
    SPK input = SPK.openForRead(inputSPK);

    SpiceWindow coverage = input.getCoverage(spacecraft.getIDCode());

    String dirName = FilenameUtils.getFullPath(inputSPK);
    if (dirName.trim().isEmpty())
      dirName = ".";
    File dir = new File(dirName);
    if (!dir.exists())
      dir.mkdirs();

    String base = FilenameUtils.getBaseName(inputSPK);

    Map<String, String> map = new TreeMap<>();
    map.put("LSK_FILE_NAME", "'" + lsk + "'");

    File sclk = new File(dir, base + ".tsc");
    map.put(sclk.exists() ? "SCLK_FILE_NAME" : "MAKE_FAKE_SCLK", "'" + sclk + "'");
    map.put("CK_TYPE", "3");
    map.put("INSTRUMENT_ID", String.format("%d", spacecraft.getIDCode() * 1000));
    map.put("REFERENCE_FRAME_NAME", "'J2000'");
    map.put("ANGULAR_RATE_PRESENT", "'MAKE UP/NO AVERAGING'");
    map.put("INPUT_TIME_TYPE", "'UTC'");
    map.put("INPUT_DATA_TYPE", "'SPICE QUATERNIONS'");
    map.put("PRODUCER_ID", "'Hari.Nair@jhuapl.edu'");

    File setupFile = new File(dir, "msopck.setup");
    try (PrintWriter pw = new PrintWriter(setupFile)) {
      pw.println("\\begindata");
      for (String key : map.keySet()) {
        pw.printf("%s = %s\n", key, map.get(key));
      }
    } catch (FileNotFoundException e) {
      logger.warn(e.getLocalizedMessage(), e);
    }

    NavigableMap<TDBTime, SpiceQuaternion> attitudeMap = new TreeMap<>(new Comparator<TDBTime>() {

      @Override
      public int compare(TDBTime o1, TDBTime o2) {
        try {
          return Double.compare(o1.getTDBSeconds(), o2.getTDBSeconds());
        } catch (SpiceErrorException e) {
          logger.error(e.getLocalizedMessage(), e);
        }
        return 0;
      }
    });

    for (int i = 0; i < coverage.card(); i++) {
      double[] interval = coverage.getInterval(i);
      double duration = interval[1] - interval[0];
      double delta = Math.min(duration / numStates, maxStep);
      int nSteps = (int) (duration / delta);
      delta = duration / (nSteps - 1);

      // pad by delta at the edges of the coverage range
      int startIndex = i == 0 ? 1 : 0;
      int stopIndex = i == coverage.card() - 1 ? nSteps - 1 : nSteps;
      for (int j = startIndex; j < stopIndex; j++) {
        double t = interval[0] + j * delta;
        TDBTime tdb = new TDBTime(t);
        attitudeMap.put(tdb, getAttitude(tdb, primaryAxis, primaryTarget, primaryVelocity,
            secondaryAxis, secondaryTarget, secondaryVelocity));
      }
    }
    File inputFile = new File(dir, "msopck.inp");
    try (PrintWriter pw = new PrintWriter(new FileWriter(inputFile))) {
      for (TDBTime t : attitudeMap.keySet()) {
        SpiceQuaternion q = attitudeMap.get(t);
        Vector3 v = q.getVector();
        pw.printf("%s %.14e %.14e %.14e %.14e\n", t.toUTCString("ISOC", 3), q.getScalar(),
            v.getElt(0), v.getElt(1), v.getElt(2));
      }
    } catch (IOException e) {
      logger.error(e.getLocalizedMessage(), e);
    }

    File ckFile = new File(dir, base + ".bc");
    return String.format("msopck %s %s %s", setupFile.getPath(), inputFile.getPath(),
        ckFile.getPath());

  }

  public static void main(String[] args) throws SpiceException {

    StringBuffer axes = new StringBuffer();
    for (AXIS axis : AXIS.values())
      axes.append(axis.name() + " ");
    String axisString = axes.toString().trim();

    Options options = new Options();
    options.addOption(Option.builder("inputSPK").required().hasArgs().desc(
        "Required.  Name of SPK to read.  The basename of this file will be used to create the ck and sclk files.  "
            + "Additional required kernels may be specified after this one, separated by whitespace.")
        .build());
    options.addOption(Option.builder("logFile").hasArg()
        .desc("If present, save screen output to log file.").build());
    options.addOption(Option.builder("logLevel").hasArg()
        .desc("If present, print messages above selected priority.  Valid values are "
            + "ALL, OFF, SEVERE, WARNING, INFO, CONFIG, FINE, FINER, or FINEST.")
        .build());
    options.addOption(Option.builder("lsk").required().hasArg()
        .desc("Required.  Path to leapseconds kernel.").build());
    options.addOption(Option.builder("maxStep").hasArg().desc(
        "Maximum size of a time step to take when evaluating states in each segment.  Default is 86400.")
        .build());
    options.addOption(Option.builder("primaryAxis").required().hasArg()
        .desc("Required.  Primary axis.  Valid values are " + axisString + ".").build());
    options.addOption(Option.builder("primaryTarget").required().hasArg()
        .desc("Required.  Name of primary target.").build());
    options.addOption(Option.builder("primaryVelocity").desc(
        "If present, use spacecraft velocity relative to primary target to align primary axis.  "
            + "Default is to align primary axis along the vector from spacecraft to primaryTarget.")
        .build());
    options.addOption(Option.builder("secondaryAxis").required().hasArg()
        .desc("Required.  Secondary axis.  Valid values are " + axisString + ".").build());
    options.addOption(Option.builder("secondaryTarget").required().hasArg()
        .desc("Required.  Name of secondary target.").build());
    options.addOption(Option.builder("secondaryVelocity").desc(
        "If present, use spacecraft velocity relative to secondary target to align secondary axis.  "
            + "Default is to align secondary axis along the vector from spacecraft to secondaryTarget.")
        .build());
    options.addOption(Option.builder("spacecraft").required().hasArg().desc(
        "Required.  NAIF name or integer ID code for the spacecraft.  The instrument ID will be set to "
            + "1000*spacecraft id and the SCLK ID will be set to spacecraft id.")
        .build());
    options.addOption(Option.builder("states").hasArg().desc(
        "Number of states to evaluate in each segment when creating the new CK.  Default is 1000.  "
            + "This may increase if maxStep is smaller than the segment duration/states.")
        .build());

    // if no arguments, print the usage and exit
    if (args.length == 0) {
      System.out.println(defaultOBJ.fullDescription(options));
      System.exit(0);
    }

    // if -shortDescription is specified, print short description and exit.
    for (String arg : args) {
      if (arg.equals("-shortDescription")) {
        System.out.println(defaultOBJ.shortDescription());
        System.exit(0);
      }
    }

    // parse the arguments
    CommandLine cl = null;
    try {
      cl = new DefaultParser().parse(options, args);
    } catch (ParseException e) {
      System.out.println(e.getMessage());
      System.out.println(defaultOBJ.fullDescription(options));
      System.exit(0);
    }

    String hostname = "unknown host";
    try {
      hostname = InetAddress.getLocalHost().getHostName();
    } catch (UnknownHostException e) {
    }

    // if (cl.hasOption("logLevel"))
    // logger.setLogLevel(Level.parse(cl.getOptionValue("logLevel").toUpperCase().trim()));
    // if (cl.hasOption("logFile"))
    // logger.setLogFile(cl.getOptionValue("logFile"));

    logger.info(String.format("Start %s + [%s] on %s", defaultOBJ.getClass().getSimpleName(),
        AppVersion.getVersionString(), hostname));
    StringBuilder sb = new StringBuilder();
    sb.append("arguments: ");
    for (String arg : args)
      sb.append(String.format("%s ", arg));
    logger.info(sb.toString());

    System.loadLibrary("JNISpice");

    String[] kernels = cl.getOptionValues("inputSPK");

    String inputSPK = kernels[0];
    for (String kernel : kernels)
      KernelDatabase.load(kernel);
    String lsk = cl.getOptionValue("lsk");
    KernelDatabase.load(lsk);

    double maxStep = cl.hasOption("maxStep") ? Integer.parseInt(cl.getOptionValue("maxStep"))
        : TimeConstants.SPD;
    int numStates = cl.hasOption("states") ? Integer.parseInt(cl.getOptionValue("states")) : 1000;

    AXIS primaryAxis = AXIS.valueOf(cl.getOptionValue("primaryAxis").toUpperCase());
    Body primaryTarget = new Body(cl.getOptionValue("primaryTarget"));
    boolean primaryVelocity = cl.hasOption("primaryVelocity");

    AXIS secondaryAxis = AXIS.valueOf(cl.getOptionValue("secondaryAxis").toUpperCase());
    Body secondaryTarget = new Body(cl.getOptionValue("secondaryTarget"));
    boolean secondaryVelocity = cl.hasOption("secondaryVelocity");

    Body spacecraft = new Body(cl.getOptionValue("spacecraft"));

    CreateCK app = new CreateCK(inputSPK, spacecraft);

    String command = app.createCK(lsk, maxStep, numStates, primaryAxis, primaryTarget,
        primaryVelocity, secondaryAxis, secondaryTarget, secondaryVelocity);

    logger.info(String.format("Command to run MSOPCK:\n%s", command));

  }

}
