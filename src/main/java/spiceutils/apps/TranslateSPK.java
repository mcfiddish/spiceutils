package spiceutils.apps;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spice.basic.AberrationCorrection;
import spice.basic.Body;
import spice.basic.KernelDatabase;
import spice.basic.ReferenceFrame;
import spice.basic.SPK;
import spice.basic.SpiceErrorException;
import spice.basic.SpiceException;
import spice.basic.StateVector;
import spice.basic.TDBTime;
import spice.basic.Time;
import spice.basic.TimeConstants;
import spiceutils.templates.SPICETool;
import spiceutils.utils.AppVersion;
import spiceutils.utils.SPKSegment;

public class TranslateSPK implements SPICETool {

  private static TranslateSPK defaultOBJ = new TranslateSPK();

  private TranslateSPK() {}

  @Override
  public String shortDescription() {
    return "Translate an SPK to type 9 or 13.";
  }

  @Override
  public String fullDescription(Options options) {
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);

    // @formatter:off
		String header = "";
		String footer = "\nThis program is used to translate SPK files from a format unsupported by Crucible "
				+ "(e.g. 14 or 21) to a type 9 or 13 SPK.\n\n"
				+ "The states for each segment in the output file are computed from the original, complete SPK, "
				+ "not from the segment.  Thus overlapping time ranges will have identical states and may not "
				+ "correspond to the states computed from the segments in the original file.";
		// @formatter:on
    pw.println(AppVersion.getVersionString() + "\n");
    HelpFormatter formatter = new org.apache.commons.cli.HelpFormatter();
    formatter.printHelp(pw, formatter.getWidth(),
        String.format("%s [options]", this.getClass().getSimpleName()), header, options,
        formatter.getLeftPadding(), formatter.getDescPadding(), footer);
    pw.flush();
    return sw.toString();
  }

  private final static int MAX_LINE_LEN = 1024;

  private String inputSPK;
  private static final Logger logger = LogManager.getLogger();

  public TranslateSPK(String inputSPK) {
    this.inputSPK = inputSPK;
  }

  public void translate(String outputSPK, String lsk, int degree, double maxStep, int numStates,
      int type) throws SpiceException {

    final String TIMFMT = new String("YYYY MON DD HR:MN:SC.### ::TDB");

    final AberrationCorrection abCorr = new AberrationCorrection("NONE");
    if (lsk != null)
      KernelDatabase.load(lsk);
    KernelDatabase.load(inputSPK);

    SPK input = SPK.openForRead(inputSPK);

    List<String> comments = new ArrayList<>();
    comments.add(String.format(
        "This type %d SPK was translated from %s.  Comments from the original file follow.", type,
        inputSPK));
    comments.add(" ");
    comments.add(" ");
    comments.add(" ");

    // Fortran doesn't like zero length strings
    for (String comment : input.readComments(MAX_LINE_LEN))
      comments.add(comment + " ");

    int commentLength = 0;
    for (String comment : comments)
      commentLength += comment.length();

    SPK output = SPK.openNew(outputSPK, input.getInternalFileName(), commentLength);
    String[] commentArray = new String[comments.size()];
    output.addComments(comments.toArray(commentArray));

    String format =
        String.format("Segment %%0%dd", (int) Math.floor(Math.log10(input.countSegments()) + 1));

    input.beginForwardSearch();

    boolean found = input.findNextArray();

    List<SPKSegment> segments = new ArrayList<>();

    logger.info("Reading " + inputSPK);

    int segno = 0;
    while (found) {
      ++segno;

      int[] ic = input.getIntegerSummaryComponent();
      double[] dc = input.getDoubleSummaryComponent();

      String segid = String.format(format, segno);
      Body target = new Body(ic[0]);
      Body center = new Body(ic[1]);
      ReferenceFrame frame = new ReferenceFrame(ic[2]);

      TDBTime first = new TDBTime(dc[0]);
      TDBTime last = new TDBTime(dc[1]);

      logger.info(String.format("%s type %d", segid, ic[3]));
      logger.debug("Body        = " + target.getName());
      logger.debug("Center      = " + center.getName());
      logger.debug("Frame       = " + frame.getName());
      if (lsk != null) {
        logger.debug("Start (UTC) = " + first.toUTCString("C", 3));
        logger.debug("Stop (UTC)  = " + last.toUTCString("C", 3));
        logger.debug("Start (TDB) = " + first.toString(TIMFMT));
        logger.debug("Stop (TDB)  = " + last.toString(TIMFMT));
      }
      segments.add(SPKSegment.builder().index(segno).target(target).center(center).frame(frame)
          .first(first).last(last).segid(segid).degree(degree).build());
      found = input.findNextArray();
    }

    logger.info("Writing " + outputSPK);
    for (SPKSegment segment : segments) {

      String segid = segment.getSegid();
      Body target = segment.getTarget();
      Body center = segment.getCenter();
      ReferenceFrame frame = segment.getFrame();
      TDBTime first = segment.getFirst();
      TDBTime last = segment.getLast();

      double startTime = first.getTDBSeconds();
      double stopTime = last.getTDBSeconds();
      double duration = stopTime - startTime;
      double thisStep = Math.min(maxStep, duration / numStates);

      int nSteps = (int) (duration / thisStep);
      thisStep = duration / (nSteps - 1);

      logger.info(String.format("%s type %d", segid, type));
      logger.debug("Body        = " + target.getName());
      logger.debug("Center      = " + center.getName());
      logger.debug("Frame       = " + frame.getName());
      if (lsk != null) {
        logger.debug("Start (UTC) = " + first.toUTCString("C", 3));
        logger.debug("Stop (UTC)  = " + last.toUTCString("C", 3));
        logger.debug("Start (TDB) = " + first.toString(TIMFMT));
        logger.debug("Stop (TDB)  = " + last.toString(TIMFMT));
      }

      logger.debug(String.format("Writing %d states of duration %f seconds.", nSteps, thisStep));

      Time[] epochs = new Time[nSteps];
      StateVector[] states = new StateVector[nSteps];

      for (int i = 0; i < nSteps; i++) {
        epochs[i] = new TDBTime(startTime + i * thisStep);
        // If CSPICE.spkvpn gets added, use that instead
        states[i] = new StateVector(target, epochs[i], frame, abCorr, center);
      }

      if (type == 9) {
        output.writeType09Segment(target, center, frame, first, last, segid, degree, nSteps, states,
            epochs);
      } else if (type == 13) {
        output.writeType13Segment(target, center, frame, first, last, segid, degree, nSteps, states,
            epochs);
      }

    }

  }

  public static void main(String[] args) throws SpiceErrorException {

    Options options = new Options();
    options.addOption(Option.builder("degree").hasArg().desc("Degree of polynomial to use.  "
        + "For type 9 degree should be odd.  For type 13 should be equivalent to 3 mod 4 (e.g. 3, 7, 11).  "
        + "Default is 7.").build());
    options.addOption(Option.builder("inputSPK").required().hasArg()
        .desc("Required.  Name of input SPK file.").build());
    options.addOption(Option.builder("logFile").hasArg()
        .desc("If present, save screen output to log file.").build());
    options.addOption(Option.builder("logLevel").hasArg()
        .desc("If present, print messages above selected priority.  Valid values are "
            + "ALL, OFF, SEVERE, WARNING, INFO, CONFIG, FINE, FINER, or FINEST.")
        .build());
    options.addOption(Option.builder("lsk").hasArg().desc(
        "Path to leapseconds kernel.  Required for printing times in diagnostic output (-logLevel FINE or higher).")
        .build());
    options.addOption(Option.builder("maxStep").hasArg().desc(
        "Maximum size of a time step to take when evaluating states in each segment.  Default is 86400.")
        .build());
    options.addOption(Option.builder("outputSPK").required().hasArg()
        .desc("Required.  Name of SPK file to write.").build());
    options.addOption(Option.builder("states").hasArg().desc(
        "Number of states to evaluate in each segment when creating the new SPK.  Default is 1000.  "
            + "This may increase if maxStep is smaller than the segment duration/states.")
        .build());
    options.addOption(Option.builder("type").hasArg()
        .desc("SPK type to write.  Allowed values are 9 or 13.  Default is 9.").build());

    // if no arguments, print the usage and exit
    if (args.length == 0) {
      System.out.println(defaultOBJ.fullDescription(options));
      System.exit(0);
    }

    // if -shortDescription is specified, print short description and exit.
    for (String arg : args) {
      if (arg.equals("-shortDescription")) {
        System.out.println(defaultOBJ.shortDescription());
        System.exit(0);
      }
    }

    // parse the arguments
    CommandLine cl = null;
    try {
      cl = new DefaultParser().parse(options, args);
    } catch (ParseException e) {
      System.out.println(e.getMessage());
      System.out.println(defaultOBJ.fullDescription(options));
      System.exit(0);
    }

    String hostname = "unknown host";
    try {
      hostname = InetAddress.getLocalHost().getHostName();
    } catch (UnknownHostException e) {
    }

    // SimpleLogger logger = SimpleLogger.getInstance();
    // if (cl.hasOption("logLevel"))
    // logger.setLogLevel(Level.parse(cl.getOptionValue("logLevel").toUpperCase().trim()));
    // if (cl.hasOption("logFile"))
    // logger.setLogFile(cl.getOptionValue("logFile"));

    logger.info(String.format("Start %s + [%s] on %s", defaultOBJ.getClass().getSimpleName(),
        AppVersion.getVersionString(), hostname));
    StringBuilder sb = new StringBuilder();
    sb.append("arguments: ");
    for (String arg : args)
      sb.append(String.format("%s ", arg));
    logger.info(sb.toString());

    System.loadLibrary("JNISpice");

    int degree = cl.hasOption("degree") ? Integer.parseInt(cl.getOptionValue("degree")) : 7;
    double maxStep = cl.hasOption("maxStep") ? Integer.parseInt(cl.getOptionValue("maxStep"))
        : TimeConstants.SPD;
    int numStates = cl.hasOption("states") ? Integer.parseInt(cl.getOptionValue("states")) : 1000;
    int type = cl.hasOption("type") ? Integer.parseInt(cl.getOptionValue("type")) : 9;

    if (type != 9 && type != 13) {
      logger.warn("Invalid SPK type " + type);
      System.exit(0);
    }

    if (type == 9)
      if (degree % 2 == 0)
        logger.warn("Bad choice of degree for type 9 SPK!");
    if (type == 13) {
      if (degree % 4 != 3)
        logger.warn("Bad choice of degree for type 13 SPK!");
    }

    String lsk = cl.hasOption("lsk") ? cl.getOptionValue("lsk") : null;
    TranslateSPK app = new TranslateSPK(cl.getOptionValue("inputSPK"));
    try {
      String outputSPK = cl.getOptionValue("outputSPK");
      if (new File(outputSPK).exists())
        logger.warn(outputSPK + " exists!  Will not overwrite.");
      else
        app.translate(outputSPK, lsk, degree, maxStep, numStates, type);
    } catch (SpiceException e) {
      logger.error(e.getLocalizedMessage(), e);
    }

  }

}
