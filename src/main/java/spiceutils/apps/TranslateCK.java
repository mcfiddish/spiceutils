package spiceutils.apps;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import spice.basic.AxisAndAngle;
import spice.basic.Body;
import spice.basic.CK;
import spice.basic.CSPICE;
import spice.basic.Instrument;
import spice.basic.KernelDatabase;
import spice.basic.Matrix33;
import spice.basic.ReferenceFrame;
import spice.basic.SCLK;
import spice.basic.SCLKTime;
import spice.basic.SpiceErrorException;
import spice.basic.SpiceException;
import spice.basic.SpiceQuaternion;
import spice.basic.TDBTime;
import spice.basic.Time;
import spice.basic.TimeConstants;
import spice.basic.Vector3;
import spiceutils.templates.SPICETool;
import spiceutils.utils.AppVersion;
import spiceutils.utils.CKSegment;

public class TranslateCK implements SPICETool {

  private static TranslateCK defaultOBJ = new TranslateCK();

  private TranslateCK() {}

  @Override
  public String shortDescription() {
    return "Translate an SPK to type 3.";
  }

  @Override
  public String fullDescription(Options options) {
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);

    // @formatter:off
		String header = "";
		String footer = "\nThis program is used to translate CK files from a format unsupported by Crucible "
				+ "(e.g. 1) to a type 3 CK.\n\n"
				+ "The states for each segment in the output file are computed from the original, complete CK, "
				+ "not from the segment.  Thus overlapping time ranges will have identical states and may not "
				+ "correspond to the states computed from the segments in the original file.";
		// @formatter:on
    pw.println(AppVersion.getVersionString() + "\n");
    HelpFormatter formatter = new org.apache.commons.cli.HelpFormatter();
    formatter.printHelp(pw, formatter.getWidth(),
        String.format("%s [options]", this.getClass().getSimpleName()), header, options,
        formatter.getLeftPadding(), formatter.getDescPadding(), footer);
    pw.flush();
    return sw.toString();
  }

  private final static int MAX_LINE_LEN = 1024;

  private String inputCK;
  private static final Logger logger = LogManager.getLogger();

  public TranslateCK(String inputCK) {
    this.inputCK = inputCK;
  }

  public void translate(String outputCK, String sclkFile, String lskFile, double maxStep,
      int numStates) throws SpiceException {

    if (lskFile != null)
      KernelDatabase.load(lskFile);
    KernelDatabase.load(inputCK);
    KernelDatabase.load(sclkFile);

    CK input = CK.openForRead(inputCK);

    List<String> comments = new ArrayList<>();
    comments.add(String.format(
        "This type 3 CK was translated from %s.  Comments from the original file follow.",
        inputCK));
    comments.add(" ");
    comments.add(" ");
    comments.add(" ");

    // Fortran doesn't like zero length strings
    for (String comment : input.readComments(MAX_LINE_LEN))
      comments.add(comment + " ");

    int commentLength = 0;
    for (String comment : comments)
      commentLength += comment.length();

    CK output = CK.openNew(outputCK, input.getInternalFileName(), commentLength);
    String[] commentArray = new String[comments.size()];
    output.addComments(comments.toArray(commentArray));

    String format =
        String.format("Segment %%0%dd", (int) Math.floor(Math.log10(input.countSegments()) + 1));

    input.beginForwardSearch();

    boolean found = input.findNextArray();

    List<CKSegment> segments = new ArrayList<>();

    logger.info("Reading " + inputCK);

    int segno = 0;
    while (found) {
      ++segno;

      int[] ic = input.getIntegerSummaryComponent();
      double[] dc = input.getDoubleSummaryComponent();

      String segid = String.format(format, segno);
      Instrument inst = new Instrument(ic[0]);
      ReferenceFrame frame = new ReferenceFrame(ic[1]);
      int type = ic[2];
      boolean avflag = (ic[3] != 0);

      SCLK instSclk = inst.getSCLK();
      Body spacecraft = new Body(instSclk.getIDCode());

      SCLKTime first = new SCLKTime(instSclk, dc[0]);
      SCLKTime last = new SCLKTime(instSclk, dc[1]);

      logger.info(String.format("%s type %d", segid, type));
      logger.debug("Instrument         = " + inst.getName());
      logger.debug("Spacecraft         = " + spacecraft.getName());
      logger.debug("CK Type            = " + type);
      logger.debug("Angular Velocities = " + avflag);
      logger.debug("Frame              = " + frame.getName());
      if (lskFile != null) {
        logger.debug("Start (UTC)        = " + new TDBTime(first).toUTCString("C", 3));
        logger.debug("Stop (UTC)         = " + new TDBTime(last).toUTCString("C", 3));
        logger.debug("Start (SCLK)       = " + first.toString());
        logger.debug("Stop (SCLK)        = " + last.toString());
      }
      segments.add(CKSegment.builder().index(segno).first(first).last(last).inst(inst).frame(frame)
          .segid(segid).build());
      found = input.findNextArray();
    }

    logger.info("Writing " + outputCK);
    for (CKSegment segment : segments) {

      String segid = segment.getSegid();
      SCLKTime first = segment.getFirst();
      SCLKTime last = segment.getLast();
      Instrument inst = segment.getInst();
      ReferenceFrame frame = segment.getFrame();
      boolean avflag = segment.isAvflag();

      double startTime = first.getContinuousTicks();
      double stopTime = last.getContinuousTicks();
      double duration = stopTime - startTime;
      double thisStep = Math.min(maxStep, duration / numStates);

      int nSteps = (int) (duration / thisStep);
      thisStep = duration / (nSteps - 1);

      logger.info(String.format("%s type 3", segid));
      logger.debug("Instrument         = " + inst.getName());
      logger.debug("Frame              = " + frame.getName());
      if (lskFile != null) {
        logger.debug("Start (UTC)        = " + new TDBTime(first).toUTCString("C", 3));
        logger.debug("Stop (UTC)         = " + new TDBTime(last).toUTCString("C", 3));
        logger.debug("Start (SCLK)       = " + first.toString());
        logger.debug("Stop (SCLK)        = " + last.toString());
      }

      logger.debug(String.format("Writing %d states of duration %f seconds.", nSteps, thisStep));

      SCLKTime[] timeTags = new SCLKTime[nSteps];
      SpiceQuaternion[] quats = new SpiceQuaternion[nSteps];
      Vector3[] avvs = new Vector3[nSteps];
      Time[] startTimes = new Time[nSteps];

      for (int i = 0; i < nSteps; i++) {
        timeTags[i] = new SCLKTime(inst.getSCLK(), startTime + i * thisStep);
        startTimes[i] = timeTags[i];
        if (avflag) {
          double[][] cmat = new double[3][3];
          double[] av = new double[3];
          double[] clkout = new double[1];
          boolean[] foundPointing = new boolean[1];
          CSPICE.ckgpav(inst.getIDCode(), timeTags[i].getContinuousTicks(), stopTime - startTime,
              frame.getName(), cmat, av, clkout, foundPointing);
          quats[i] = new SpiceQuaternion(new Matrix33(cmat));
          avvs[i] = new Vector3(av);
        } else {
          double[][] cmat = new double[3][3];
          double[] clkout = new double[1];
          boolean[] foundPointing = new boolean[1];

          CSPICE.ckgp(inst.getIDCode(), timeTags[i].getContinuousTicks(), stopTime - startTime,
              frame.getName(), cmat, clkout, foundPointing);
          quats[i] = new SpiceQuaternion(new Matrix33(cmat));

          double prevTicks = timeTags[i].getContinuousTicks() - thisStep / 2;
          double nextTicks = timeTags[i].getContinuousTicks() + thisStep / 2;

          CSPICE.ckgp(inst.getIDCode(), prevTicks, stopTime - startTime, frame.getName(), cmat,
              clkout, foundPointing);
          Matrix33 prevRot = new Matrix33(cmat);

          CSPICE.ckgp(inst.getIDCode(), nextTicks, stopTime - startTime, frame.getName(), cmat,
              clkout, foundPointing);
          Matrix33 nextRot = new Matrix33(cmat);

          Matrix33 prevToNext = nextRot.mxmt(prevRot);

          AxisAndAngle aaa = new AxisAndAngle(prevToNext);
          avvs[i] = aaa.getAxis().hat().scale(aaa.getAngle() / thisStep);
        }
      }

      output.writeType03Segment(first, last, inst, frame, true, segid, timeTags, quats, avvs,
          startTimes);
    }

  }

  public static void main(String[] args) throws SpiceErrorException {

    Options options = new Options();
    options.addOption(Option.builder("inputCK").required().hasArg()
        .desc("Required.  Name of input CK file.").build());
    options.addOption(Option.builder("logFile").hasArg()
        .desc("If present, save screen output to log file.").build());
    options.addOption(Option.builder("logLevel").hasArg()
        .desc("If present, print messages above selected priority.  Valid values are "
            + "ALL, OFF, SEVERE, WARNING, INFO, CONFIG, FINE, FINER, or FINEST.")
        .build());
    options.addOption(Option.builder("lsk").hasArg().desc(
        "Path to leapseconds kernel.  Required for printing times in diagnostic output (-logLevel FINE or higher).")
        .build());
    options.addOption(Option.builder("maxStep").hasArg().desc(
        "Maximum size of a time step in spacecraft ticks to take when evaluating states in each segment.  "
            + "Default is 86400.")
        .build());
    options.addOption(Option.builder("outputCK").required().hasArg()
        .desc("Required.  Name of CK file to write.").build());
    options.addOption(Option.builder("sclk").required().hasArg()
        .desc("Required.  Name of SCLK to use with input CK file.").build());

    options.addOption(Option.builder("states").hasArg().desc(
        "Number of states to evaluate in each segment when creating the new CK.  Default is 1000.  "
            + "This may increase if maxStep is smaller than the segment duration/states.")
        .build());

    // if no arguments, print the usage and exit
    if (args.length == 0) {
      System.out.println(defaultOBJ.fullDescription(options));
      System.exit(0);
    }

    // if -shortDescription is specified, print short description and exit.
    for (String arg : args) {
      if (arg.equals("-shortDescription")) {
        System.out.println(defaultOBJ.shortDescription());
        System.exit(0);
      }
    }

    // parse the arguments
    CommandLine cl = null;
    try {
      cl = new DefaultParser().parse(options, args);
    } catch (ParseException e) {
      System.out.println(e.getMessage());
      System.out.println(defaultOBJ.fullDescription(options));
      System.exit(0);
    }

    String hostname = "unknown host";
    try {
      hostname = InetAddress.getLocalHost().getHostName();
    } catch (UnknownHostException e) {
    }

    // SimpleLogger logger = SimpleLogger.getInstance();
    // if (cl.hasOption("logLevel"))
    // logger.setLogLevel(Level.parse(cl.getOptionValue("logLevel").toUpperCase().trim()));
    // if (cl.hasOption("logFile"))
    // logger.setLogFile(cl.getOptionValue("logFile"));

    logger.info(String.format("Start %s + [%s] on %s", defaultOBJ.getClass().getSimpleName(),
        AppVersion.getVersionString(), hostname));
    StringBuilder sb = new StringBuilder();
    sb.append("arguments: ");
    for (String arg : args)
      sb.append(String.format("%s ", arg));
    logger.info(sb.toString());

    System.loadLibrary("JNISpice");

    double maxStep = cl.hasOption("maxStep") ? Integer.parseInt(cl.getOptionValue("maxStep"))
        : TimeConstants.SPD;
    int numStates = cl.hasOption("states") ? Integer.parseInt(cl.getOptionValue("states")) : 1000;

    String lsk = cl.hasOption("lsk") ? cl.getOptionValue("lsk") : null;
    String sclk = cl.getOptionValue("sclk");
    TranslateCK app = new TranslateCK(cl.getOptionValue("inputCK"));
    try {
      String outputCK = cl.getOptionValue("outputCK");
      if (new File(outputCK).exists())
        logger.warn(outputCK + " exists!  Will not overwrite.");
      else
        app.translate(outputCK, sclk, lsk, maxStep, numStates);
    } catch (SpiceException e) {
      logger.error(e.getLocalizedMessage(), e);
    }

  }

}
