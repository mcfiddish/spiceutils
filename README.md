# SPICEUtils

Utilities using NAIF's JNISpice

## Quick Start

### Prerequisites

The SPICEUtils package requires Java 11 or later.  Some freely available versions are

* [AdoptOpenJDK](https://adoptopenjdk.net/)
* [Amazon Corretto](https://aws.amazon.com/corretto/)
* [Azul Zulu](https://www.azul.com/downloads/zulu-community/?package=jdk)

### Build the package

This is an optional step if you don't want to use the prebuilt package.  You can download the source distribution from this page or check out the code:

    git clone http://quiver.jhuapl.edu/nairah1/spiceutils.git

Maven must be installed to build the software.  You will need to be on the APL internal network to access all of the dependencies.

    cd spiceutils
    ./mkPackage.bash

This will create executable and source packages in the dist directory, named SPICEUtils-YYYY.MM.DD.tar.gz and SPICEUtils-YYYY.MM.DD-src.tar.gz 

### Install the executable package

    cd (your destination directory)
    tar xfz (path to spiceutils)/dist/SPICEUtils-YYYY.MM.DD.tar.gz
    
The scripts/ directory contains all of the applications in the package.  Running without any arguments will display a usage message.

The doc/ directory contains documentation including a javadoc directory.
